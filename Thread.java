package examen;

public class Thread {
    public class Main {
        public static void main(String[] args) {
            MultipleThread NewThread1 = new MultipleThread("NewThread1");
            MultipleThread NewThread2 = new MultipleThread("NewThread2");
            MultipleThread NewThread3 = new MultipleThread("NewThread3");
            MultipleThread NewThread4 = new MultipleThread("NewThread4");
            Thread thread1 = new Thread(NewThread1);
            Thread thread2 = new Thread(NewThread2);
            Thread thread3 = new Thread(NewThread3);
            Thread thread4 = new Thread(NewThread4);

            thread1.start();
            thread2.start();
            thread3.start();
            thread4.start();
        }
    }

    class MultipleThread implements Runnable {
        private String name;
        private int number;

        public MultipleThread(String name) {
            this.name = name;
            this.number = 1;
        }
        @Override
        public void run() {

            while (this.number <= 10) { //each thread should write 10 messages
            while (this.number <= 10) { //each thread should write 10 messages

                System.out.println("["+this.name+"]" + " - [" + System.currentTimeMillis() + "]");
                try {
                    Thread.sleep(1000);
                    this.number++;
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
        }
    }
}
